import Survey from "@models/survey";
import { connectToDB } from "@utils/database";

export const POST = async (request: Request) => {
  const surveyBody = await request.json();

  try {
    await connectToDB();
    const newSurvey = new Survey(surveyBody);

    await newSurvey.save();
    return new Response(JSON.stringify(newSurvey), { status: 201 })
  } catch (error) {
    return new Response("Failed to create a new survey", { status: 500 });
  }
}