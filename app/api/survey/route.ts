import Survey from "@models/survey";
import { connectToDB } from "@utils/database";

export const GET = async () => {
  try {
    await connectToDB()

    const prompts = await Survey.find()
    return new Response(JSON.stringify(prompts), { status: 200 })
  } catch (error) {
    return new Response("Failed to fetch all surveys", { status: 500 })
  }
} 
