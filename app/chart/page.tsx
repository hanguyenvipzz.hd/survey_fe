"use client";

import { Tooltip } from "antd";
import { useEffect, useState } from "react";
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Legend } from "recharts";

// const languageMap = new Map([
//   ["C/C++", 12],
//   ["Javascript", 0],
//   ["Python", 0],
//   ["Java", 0],
//   ["C#", 0],
//   ["PHP", 0],
// ]);

const Chart = () => {
  const [surveys, setSurveys] = useState<any[]>([]);

  useEffect(() => {
    const fetchSurveys = async () => {
      const response = await fetch(`/api/survey`);
      const data = await response.json();

      setSurveys(data);
    };
    fetchSurveys();
  }, []);

  // surveys.forEach(item => {
  //   if(item.languages.length > 0){
  //     for (let index = 0; index < item.languages.length; index++) {
  //       console.log('aaaaaaa: ', languageMap.get(item.languages[index])!!);
  //       languageMap.set(item.languages[index], +languageMap.get(item.languages[index])!! + 1)
  //     }
  //   }
  // })
  // const languageGroup = Array.from(languageMap, ([name, value]) => ({
  //   name,
  //   value,
  // }));
  // console.log('languageGroup: ', languageGroup)

  return (
    <BarChart
      width={600}
      height={300}
      data={surveys}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="age" fill="#82ca9d" />
    </BarChart>
  );
};

export default Chart;
