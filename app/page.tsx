const Home = () => (
  <section className="w-full flex-center flex-col">
    <h1 className="head_text text-center">
      Survey
      <br className="max-md:hidden" />
      <span className="orange_gradient text-center">Test</span>
    </h1>
    <p className="desc text-center">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit
      alias minima soluta quibusdam odio amet ab qui assumenda? Temporibus
      provident consectetur numquam explicabo doloribus voluptate voluptas
      molestias incidunt ullam delectus!
    </p>
  </section>
);

export default Home;
