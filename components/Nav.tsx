"use client";

import Link from "next/link";

const Nav = () => {
  return (
    <nav className="flex flex-between gap-2 mb-16 pt-3">
      <Link href="/" className="black_btn">
        Home
      </Link>
      <Link href="/survey" className="black_btn">
        Survey
      </Link>
      <Link href="/chart" className="black_btn">
        Chart
      </Link>
    </nav>
  );
};

export default Nav;
