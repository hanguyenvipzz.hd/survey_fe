"use client";

import React from "react";
import {
  Button,
  Checkbox,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  notification,
} from "antd";

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 18 },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not a valid email!",
    number: "${label} is not a valid number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const languageOptions = [
  { label: "C/C++", value: "C/C++" },
  { label: "Javascript", value: "Javascript" },
  { label: "Python", value: "Python" },
  { label: "Java", value: "Java" },
  { label: "C#", value: "C#" },
  { label: "PHP", value: "PHP" },
];

const SurveyForm: React.FC = () => {
  const [form] = Form.useForm();
  const [api, contextHolder] = notification.useNotification();

  const onFinish = async ({ survey }: any) => {
    console.log("survey: ", survey);
    try {
      const response = await fetch("/api/survey/new", {
        method: "POST",
        body: JSON.stringify(survey),
      });

      if (response.ok) {
        console.log("SUCCESSFULLY!!");
        api["success"]({
          message: "Create survey",
          description: "Success",
        });
        form.resetFields();
      } else {
        api["error"]({
          message: "Create survey",
          description: response.statusText,
        });
      }
    } catch (error: any) {
      console.log(error.message);
      api["error"]({
        message: "Create survey",
        description: error.message,
      });
    }
  };

  return (
    <>
      {contextHolder}
      <Row className="flex justify-center">
        <Form
          {...layout}
          form={form}
          name="nest-messages"
          layout="vertical"
          onFinish={onFinish}
          style={{ width: "50vw" }}
          validateMessages={validateMessages}
        >
          <Form.Item
            name={["survey", "name"]}
            label="Name"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={["survey", "email"]}
            label="Email"
            rules={[{ type: "email", required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={["survey", "age"]}
            label="Age"
            rules={[{ type: "number", min: 0, max: 99 }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item
            name={["survey", "level"]}
            label="Level"
            rules={[{ required: true }]}
          >
            <Select placeholder="Level" allowClear>
              <Option value="fresher">Fresher</Option>
              <Option value="junior">Junior</Option>
              <Option value="middle">Middle</Option>
              <Option value="senior">Senior</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Languages known"
            name={["survey", "languages"]}
            valuePropName="checked"
            rules={[{ required: true }]}
          >
            <Checkbox.Group options={languageOptions} />
          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol }}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ backgroundColor: "#1677ff" }}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Row>
    </>
  );
};

export default SurveyForm;
