import { Schema, model, models } from 'mongoose';

const SurveySchema = new Schema({
  email: {
    type: String,
    unique: [true, 'Email already exists!'],
    required: [true, 'Email is required!'],
  },
  name: {
    type: String,
    required: [true, 'Name is required!'],
  },
  age: {
    type: String,
  },
  level: {
    type: String,
    required: [true, 'Level is required!'],
  },
  languages: [{
    type: String
  }]
});

const Survey = models.Survey || model("Survey", SurveySchema);

export default Survey;